function calculerCarre(nombre, callback) {
    if (typeof nombre !== "number") {
        // si le paramètre "nombre" n'est pas de type number, on passe en argument une instance de Error à la fonction de callback
        callback(new Error("Un argument de type nombre est attendu"));
    } else {
        // si le paramètre "nombre" est de type number, on passe en premier argument la valeur null et en second argument le résultat à la fonction de callback
        let carre = nombre * nombre;
        callback(null, carre);
    }
}
function carreCallback(err, data) {
    if (err !== null) {
        // la fonction de callback affiche le message d'erreur
        console.log(String(err));
    } else {
        // la fonction de callback affiche le résultat
        console.log(data);
    }
}
calculerCarre(5, carreCallback);
calculerCarre("a", carreCallback);

function jouer() {
    return new Promise(function (resolve, reject) {
        // tirage d'un nombre aléatoire
        let resultat = Math.random();
        console.log("Le résultat est : " + resultat);
        if (resultat > 0.5) {
            resolve("Bravo vous avez gagné"); // si le nombre obtenu est supérieur à 0,5 la promesse est tenue
        } else {
            reject(new Error("Désolé vous avez perdu")); // si le nombre est inférieur à 0,5 la promesse rejetée
        }
    })
}
let promesse = jouer();
promesse.then(callback_Succes)
    .catch(callback_Echec);
function callback_Succes(result) {
    console.log(result);
}
function callback_Echec(error) {
    console.log(error);
}

// // définition de la prommesse pour se connecter au serveur
// function seConnecter(param) {
//     return new Promise(function (resolve, reject) {
//         // tentative de connexion avec le serveur
//         if (connexion_Ok) {
//             resolve("Connexion établie avec le serveur");
//         } else {
//             reject(new Error("Echec de connexion avec le serveur"));
//         }
//     });
// }
// // définition de la promesse pour envoyer une requête au serveur
// function envoyerRequete(requete) {
//     return new Promise(function (resolve, reject) {
//         // on envoie une requête au serveur et on attend la réponse
//         if (reponse_OK) {
//             resolve(reponse);
//         } else {
//             reject(new Error("Le traitement de la requête a échoué"));
//         }
//     });
// }
// // création des promesses
// const promesse_connexion = seConnecter(/*paramètres de connexion*/);
// const promesse_requete = envoyerRequete(/*requête*/);
// // on enchaine l'exécution des promesses de manière à ce que l'envoi d'une requête au serveur ne soit effectué qu'une fois connecté au serveur
// promesse_connexion.then(function (result) {
//     console.log(result);
//     return promesse_requete;
// })
//     .then(function (result) {
//         console.log(result);
//     })
//     .catch(function (error) {
//         // dès qu'une promesse est rejetée, on affiche le message d'erreur correspondant
//         console.log(String(error));
//     }