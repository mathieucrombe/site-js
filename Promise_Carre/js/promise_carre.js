let secondeT = prompt("Votre Nombre de Seconde Total");

function calculerCarre(nombre) {
    return new Promise(function (resolve, reject) {
        if (typeof nombre !== "number") {
            // si le paramètre "nombre" n'est pas de type number, on passe en argument une instance de Error à la fonction de callback
            reject(new Error("Un argument de type nombre est attendu"));
        } else {
            // si le paramètre "nombre" est de type number, on passe en premier argument la valeur null et en second argument le résultat à la fonction de callback
            let carre = nombre * nombre;
            resolve(carre);
        }
    })
}
let promesse = calculerCarre(secondeT);
promesse.then(callback_Succes)
    .catch(callback_Echec);
function callback_Succes(result) {
    console.log(result);
}
function callback_Echec(error) {
    console.log(String(error));
}