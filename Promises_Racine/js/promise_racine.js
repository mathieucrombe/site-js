let chiffre = 25;
// définition de la prommesse pour se connecter au serveur
function verifierNombre(prmchiffre) {
    return new Promise(function (resolve, reject) {
        // tentative de connexion avec le serveur
        if (typeof prmchiffre !== "number") {
            reject(new Error("Un argument de type nombre est attendu"));
        } else {
            resolve(prmchiffre);
        }
    });
}
// définition de la promesse pour envoyer une requête au serveur
function calculerRacine(prmchiffre) {
    return new Promise(function (resolve, reject) {
        // on envoie une requête au serveur et on attend la réponse
        if (chiffre < 0) {
            reject(new Error("Le nombre passé en argument doit être positif ou nul"));
        } else {
            let racine = Math.sqrt(prmchiffre);
            resolve(racine);
        }
    });
}
// création des promesses
const promesse_nombre = verifierNombre(chiffre);
const promesse_racine = calculerRacine(chiffre);
// on enchaine l'exécution des promesses de manière à ce que l'envoi d'une requête au serveur ne soit effectué qu'une fois connecté au serveur
promesse_nombre.then(function (result) {
    return promesse_racine;
})
    .then(function (result) {
        console.log("la racine carré de " + chiffre + " est de " + result);
    })
    .catch(function (error) {
        // dès qu'une promesse est rejetée, on affiche le message d'erreur correspondant
        console.log(String(error));
    })

