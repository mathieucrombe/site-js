/*
Auteur : Mathieu Crombe
Date : 26/04/2021
*/
//version 2

function patient(prmNom, prmPrenom, prmAge, prmSexe, prmTaille, prmPoids) {
    this.nom = prmNom,
        this.prenom = prmPrenom,
        this.age = prmAge,
        this.sexe = prmSexe,
        this.taille = prmTaille, //cm
        this.poids = prmPoids, //kg
        this.decrire = function () {
            let genre = this.sexe;
            let description = "";
            if ((genre == 'Masculin') || (genre == 'masculin')) {
                description = "le patient " + this.prenom + " " + this.nom + " de sexe " + this.sexe + " est agé de " + this.age + " ans. il mesure " + this.taille + " cm et pèse " + this.poids + " kg";
            } else if ((genre == 'Feminin') || (genre == 'feminin')) {
                description = "la patiente " + this.prenom + " " + this.nom + " de sexe " + this.sexe + " est agée de " + this.age + " ans. elle mesure " + this.taille + " cm et pèse " + this.poids + " kg";
            }

            return description;
        }
    this.definir_imc = function (prmObj) {


        function CalculerImc() {
            let calculimc = 0;
            calculimc = prmObj.poids / ((prmObj.taille / 100) * (prmObj.taille / 100));
            return calculimc;
        }
        let imc = CalculerImc();
        let interpret = CalculerImc(imc);

        return interpret;
    }

    this.definir_corpulence = function (prmObj) {


        function CalculerImc() {
            let calculimc = 0;
            calculimc = prmObj.poids / ((prmObj.taille / 100) * (prmObj.taille / 100));
            return calculimc;
        }
        let imc = CalculerImc().toFixed(2);
        let genre = prmObj.sexe;
        function interpreter_IMC(prmIMC) {
            let interpretation = "";
            if ((genre == 'Masculin') || (genre == 'masculin')) {
                if (prmIMC < 16.5) {
                    interpretation = "denutrition";         //denutritio,

                } else if ((prmIMC >= 16.5) && (prmIMC < 18.5)) {
                    interpretation = "maigreur";            //maigreur

                } else if ((prmIMC >= 18.5) && (prmIMC < 25)) {
                    interpretation = "corpulence normal";   //normal

                } else if ((prmIMC >= 25) && (prmIMC < 30)) {
                    interpretation = "surpoids";            //surpoids

                } else if ((prmIMC >= 30) && (prmIMC < 35)) {
                    interpretation = "obésité modéré";      //obésité Modéré

                } else if ((prmIMC >= 35) && (prmIMC <= 40)) {
                    interpretation = "obésité severe";      //obésité severe

                } else if (prmIMC > 40) {
                    interpretation = "Obésité morbide";      //morbide

                }
            } else if ((genre == 'Feminin') || (genre == 'feminin')) {
                if (prmIMC < 14.5) {
                    interpretation = "denutrition";       //denutritio,
                } else if ((prmIMC >= 14.5) && (prmIMC < 16.5)) {
                    interpretation = "maigreur";          //maigreur
                } else if ((prmIMC >= 16.5) && (prmIMC < 23)) {
                    interpretation = "corpulence normal"; //normal
                } else if ((prmIMC >= 23) && (prmIMC < 28)) {
                    interpretation = "surpoids";          //surpoids
                } else if ((prmIMC >= 28) && (prmIMC < 33)) {
                    interpretation = "obésité modéré";    //obésité Modéré
                } else if ((prmIMC >= 33) && (prmIMC <= 38)) {
                    interpretation = "obésité severe";    //obésité severe
                } else if (prmIMC > 38) {
                    interpretation = "Obésité morbide";    //morbide
                }
            }
            return interpretation;

        }
        let interpret = interpreter_IMC(imc);

        return interpret;
    }



}
function ajouter_Patient(prmPatient) {

    let imc = prmPatient.definir_imc(prmPatient).toFixed(2);
    interpret = prmPatient.definir_corpulence(prmPatient);
    const tableau = document.querySelector('table');
    const tab = document.createElement('tr');

    const nom = document.createElement('td');
    nom.textContent += prmPatient.nom;
    tab.append(nom);
    const prenom = document.createElement('td');
    prenom.textContent += prmPatient.prenom;
    tab.append(prenom);
    const age = document.createElement('td');
    age.textContent += prmPatient.age;
    tab.append(age);
    const sexe = document.createElement('td');
    sexe.textContent += prmPatient.sexe;
    tab.append(sexe);
    const taille = document.createElement('td');
    taille.textContent += prmPatient.taille;
    tab.append(taille);
    const poid = document.createElement('td');
    poid.textContent += prmPatient.poids;
    tab.append(poid);
    const IMC = document.createElement('td');
    const inter = document.createElement('td');

    if ((prmPatient.sexe == 'Masculin') || (prmPatient.sexe == 'masculin')) {
        if (imc < 16.5) {
            IMC.style.color = '#FF6600';
            inter.style.color = '#FF6600';
        } else if ((imc >= 16.5) && (imc < 18.5)) {
            IMC.style.color = '#FFCC00';
            inter.style.color = '#FFCC00';
        } else if ((imc >= 18.5) && (imc < 25)) {
            IMC.style.color = '#008000';
            inter.style.color = '#008000';
        } else if ((imc >= 25) && (imc < 30)) {
            IMC.style.color = '#FFCC00';
            inter.style.color = '#FFCC00';
        } else if ((imc >= 30) && (imc < 35)) {
            IMC.style.color = '#FF6600';
            inter.style.color = '#FF6600';
        } else if ((imc >= 35) && (imc <= 40)) {
            IMC.style.color = '#FF0000';
            inter.style.color = '#FF0000';
        } else if (imc > 40) {
            IMC.style.color = '#660000';
            inter.style.color = '#660000';
        }
    } else if ((prmPatient.sexe == 'Feminin') || (prmPatient.sexe == 'feminin')) {
        if (imc < 14.5) {
            IMC.style.color = '#FF6600';
            inter.style.color = '#FF6600';
        } else if ((imc >= 14.5) && (imc < 16.5)) {
            IMC.style.color = '#FFCC00';
            inter.style.color = '#FFCC00';
        } else if ((imc >= 16.5) && (imc < 23)) {
            IMC.style.color = '#008000';
            inter.style.color = '#008000';
        } else if ((imc >= 23) && (imc < 28)) {
            IMC.style.color = '#FFCC00';
            inter.style.color = '#FFCC00';
        } else if ((imc >= 28) && (imc < 33)) {
            IMC.style.color = '#FF6600';
            inter.style.color = '#FF6600';
        } else if ((imc >= 33) && (imc <= 38)) {
            IMC.style.color = '#FF0000';
            inter.style.color = '#FF0000';
        } else if (imc > 38) {
            IMC.style.color = '#660000';
            inter.style.color = '#660000';
        }
    }
    IMC.textContent += imc;
    inter.textContent += interpret;
    tab.append(IMC);
    tab.append(inter);
    tableau.append(tab);


}

let objPatient1 = new patient('Dupond', 'Jean', 22, 'Masculin', 22, 22);
let objPatient2 = new patient('Martin', 'Eric', 42, 'Masculin', 165, 90);
let objPatient3 = new patient('Moulin', 'Isabelle', 46, 'feminin', 158, 74);
let objPatient4 = new patient('Verwaerde', 'Paul', 55, 'Masculin', 177, 66);
let objPatient5 = new patient('Lejeune', 'Dominique', 60, 'feminin', 163, 54);
let objPatient6 = new patient('Chevalier', 'Bernard', 63, 'Masculin', 158, 78);
let objPatient7 = new patient('Durand', 'Louise', 35, 'feminin', 170, 82);
let tabPatients = new Array(objPatient1, objPatient2, objPatient3, objPatient4, objPatient5, objPatient6, objPatient7);

let interpret = '';
for (let index = 0; index < 7; index++) {
    ajouter_Patient(tabPatients[index]);
    console.log(tabPatients[index].decrire());

}


