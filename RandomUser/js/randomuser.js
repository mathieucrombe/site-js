
let url = "https://randomuser.me/api/?results=5";
fetch(url)
    .then(function (reponse) {
        console.log("Le serveur a répondu avec le code " + reponse.status);
        if (reponse.ok === true) {
            console.log("La requête a été traitée avec succès");
        } else {
            console.log("La requête n'a pas pu traitée correctement");
        }
        // on retourne la promesse en utilisant la méthode json()
        return reponse.json();
    })
    .then(function (reponse) {
        // la promesse retournée par json() a été résolue, on récupère la réponse dans un objet au format JSON
        console.log(reponse);
        let listUser = reponse["results"];

        listUser.forEach(element => {
            console.log(element);
            let nom = element.name.last;
            let prenom = element.name.first;
            console.log("L'utilisateur  a comme prénom " + prenom + " et comme nom " + nom + ".");

        });


    })
    .catch(function (error) {
        console.log("la requête n'a pas abouti");
    });