

function patient(prmNom, prmPrenom, prmAge, prmSexe, prmTaille, prmPoids) {
    this.nom = prmNom,
        this.prenom = prmPrenom,
        this.age = prmAge,
        this.sexe = prmSexe,
        this.taille = prmTaille, //cm
        this.poids = prmPoids, //kg
        this.decrire = function () {
            let genre = this.sexe;
            let description = "";
            if ((genre == 'Masculin') || (genre == 'masculin')) {
                description = "le patient " + this.prenom + " " + this.nom + " de sexe " + this.sexe + " est agé de " + this.age + " ans. il mesure " + this.taille + " cm et pèse " + this.poids + " kg";
            } else if ((genre == 'Feminin') || (genre == 'feminin')) {
                description = "la patiente " + this.prenom + " " + this.nom + " de sexe " + this.sexe + " est agée de " + this.age + " ans. elle mesure " + this.taille + " cm et pèse " + this.poids + " kg";
            }

            return description;
        }
    this.definir_imc = function (prmObj) {


        function CalculerImc() {
            let calculimc = 0;
            calculimc = prmObj.poids / ((prmObj.taille / 100) * (prmObj.taille / 100));
            return calculimc;
        }
        let imc = CalculerImc();
        let interpret = CalculerImc(imc);

        return interpret;
    }

    this.definir_corpulence = function (prmObj) {


        function CalculerImc() {
            let calculimc = 0;
            calculimc = prmObj.poids / ((prmObj.taille / 100) * (prmObj.taille / 100));
            return calculimc;
        }
        let imc = CalculerImc().toFixed(2);
        let genre = prmObj.sexe;
        function interpreter_IMC(prmIMC) {
            let interpretation = "";
            if ((genre == 'Masculin') || (genre == 'masculin')) {
                if (prmIMC < 16.5) {
                    interpretation = "denutrition";         //denutritio,
                } else if ((prmIMC >= 16.5) && (prmIMC < 18.5)) {
                    interpretation = "maigreur";            //maigreur
                } else if ((prmIMC >= 18.5) && (prmIMC < 25)) {
                    interpretation = "corpulence normal";   //normal
                } else if ((prmIMC >= 25) && (prmIMC < 30)) {
                    interpretation = "surpoids";            //surpoids
                } else if ((prmIMC >= 30) && (prmIMC < 35)) {
                    interpretation = "obésité modéré";      //obésité Modéré
                } else if ((prmIMC >= 35) && (prmIMC <= 40)) {
                    interpretation = "obésité severe";      //obésité severe
                } else if (prmIMC > 40) {
                    interpretation = "Obésité morbide";      //morbide
                }
            } else if ((genre == 'Feminin') || (genre == 'feminin')) {
                if (prmIMC < 14.5) {
                    interpretation = "denutrition";       //denutritio,
                } else if ((prmIMC >= 14.5) && (prmIMC < 16.5)) {
                    interpretation = "maigreur";          //maigreur
                } else if ((prmIMC >= 16.5) && (prmIMC < 23)) {
                    interpretation = "corpulence normal"; //normal
                } else if ((prmIMC >= 23) && (prmIMC < 28)) {
                    interpretation = "surpoids";          //surpoids
                } else if ((prmIMC >= 28) && (prmIMC < 33)) {
                    interpretation = "obésité modéré";    //obésité Modéré
                } else if ((prmIMC >= 33) && (prmIMC <= 38)) {
                    interpretation = "obésité severe";    //obésité severe
                } else if (prmIMC > 38) {
                    interpretation = "Obésité morbide";    //morbide
                }
            }
            return interpretation;

        }
        let interpret = interpreter_IMC(imc);

        return interpret;
    }



}

let objPatient = new patient('Dupond', 'Jean', 30, 'Masculin', 180, 85);
let imc = objPatient.definir_imc(objPatient).toFixed(2);
let interpret = '';
interpret = objPatient.definir_corpulence(objPatient);


const nom = document.getElementById('nom');
nom.textContent += objPatient.nom;
const prenom = document.getElementById('prenom');
prenom.textContent += objPatient.prenom;
const age = document.getElementById('age');
age.textContent += objPatient.age;
const sexe = document.getElementById('sexe');
sexe.textContent += objPatient.sexe;
const taille = document.getElementById('taille');
taille.textContent += objPatient.taille;
const poid = document.getElementById('poid');
poid.textContent += objPatient.poids;
const IMC = document.getElementById('imc');
const inter = document.getElementById('interpretation');
inter.textContent += interpret;


if ((objPatient.sexe == 'Masculin') || (objPatient.sexe == 'masculin')) {
    if (imc < 16.5) {
        IMC.style.color = '#FF6600';
        inter.style.color = '#FF6600';
    } else if ((imc >= 16.5) && (imc < 18.5)) {
        IMC.style.color = '#FFCC00';
        inter.style.color = '#FFCC00';
    } else if ((imc >= 18.5) && (imc < 25)) {
        IMC.style.color = '#008000';
        inter.style.color = '#008000';
    } else if ((imc >= 25) && (imc < 30)) {
        IMC.style.color = '#FFCC00';
        inter.style.color = '#FFCC00';
    } else if ((imc >= 30) && (imc < 35)) {
        IMC.style.color = '#FF6600';
        inter.style.color = '#FF6600';
    } else if ((imc >= 35) && (imc <= 40)) {
        IMC.style.color = '#FF0000';
        inter.style.color = '#FF0000';
    } else if (imc > 40) {
        IMC.style.color = '#660000';
        inter.style.color = '#660000';
    }
} else if ((objPatient.sexe == 'Feminin') || (objPatient.sexe == 'feminin')) {
    if (imc < 14.5) {
        IMC.style.color = '#FF6600';
        inter.style.color = '#FF6600';
    } else if ((imc >= 14.5) && (imc < 16.5)) {
        IMC.style.color = '#FFCC00';
        inter.style.color = '#FFCC00';
    } else if ((imc >= 16.5) && (imc < 23)) {
        IMC.style.color = '#008000';
        inter.style.color = '#008000';
    } else if ((imc >= 23) && (imc < 28)) {
        IMC.style.color = '#FFCC00';
        inter.style.color = '#FFCC00';
    } else if ((imc >= 28) && (imc < 33)) {
        IMC.style.color = '#FF6600';
        inter.style.color = '#FF6600';
    } else if ((imc >= 33) && (imc <= 38)) {
        IMC.style.color = '#FF0000';
        inter.style.color = '#FF0000';
    } else if (imc > 38) {
        IMC.style.color = '#660000';
        inter.style.color = '#660000';
    }
}

IMC.textContent += imc;



console.log(objPatient.decrire());
console.log(interpret);
console.log(imc);

