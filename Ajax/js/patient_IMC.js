// // on déclare une chaine de caractères contenant l'URL du service web à interroger
// let url = "https://randomuser.me/api/?results=1";
// // appel de la fonction globale fetch() pour envoyer la requête et traitement de la promesse
// fetch(url)
// // la promesse est résolue, traitement de la réponse
// .then(function(reponse){
// // on affiche le code HTTP de la réponse du serveur grâce à la propriété
// status
// console.log("Le serveur a répondu avec le code " + reponse.status);
// // on vérifie avec la propriété ok si la requête a été traitée avec succés
// if(reponse.ok === true) {
// console.log("La requête a été traitée avec succès");
// } else {
// console.log("La requête n'a pas pu traitée correctement");
// }
// })
// // la promesse a été rejetée, traitement de l'erreur
// .catch(function(error){
// console.log("La requête n'a pas abouti");
// });

let url = "https://randomuser.me/api/?results=5";
fetch(url)
    .then(function (reponse) {
        console.log("Le serveur a répondu avec le code " + reponse.status);
        if (reponse.ok === true) {
            console.log("La requête a été traitée avec succès");
        } else {
            console.log("La requête n'a pas pu traitée correctement");
        }
        // on retourne la promesse en utilisant la méthode json()
        return reponse.json();
    })
    .then(function (reponse) {
        // la promesse retournée par json() a été résolue, on récupère la réponse dans un objet au format JSON
         let listUser = reponse["results"];
         listUser.forEach(element => {
             console.log("La personne s'appele "+element.name.title+" "+element.name.last+" "+element.name.first)
         });
    })
    .catch(function (error) {
        console.log("la requête n'a pas abouti");
    });