const btn1 = document.getElementById('btnChercher'); // référence vers le bouton
const resultat = document.getElementById('divResult'); // référence vers le résultat


btn1.addEventListener('click', chercher);

function chercher() { // on utilise la méthode addEventListener() pour gérer le clic sur le bouton

    const input = document.getElementById('inputNom'); // référence vers le la valeur chercher
    let result = input.value;
    let url = "https://geo.api.gouv.fr/communes?nom=" + result;

    fetch(url)
        .then(function (reponse) {
            console.log("Le serveur a répondu avec le code " + reponse.status);
            if (reponse.ok === true) {
                console.log("La requête a été traitée avec succès");
            } else {
                console.log("La requête n'a pas pu traitée correctement");
            }
            // on retourne la promesse en utilisant la méthode json()
            return reponse.json();
        })

        .then(function (reponse) {
            // la promesse retournée par json() a été résolue, on récupère la réponse dans un objet au format JSON
            // console.log(reponse);
            let listCommune = reponse;

            listCommune.forEach(element => {
                console.log(element);
                let affichage = element.code +" "+ element.nom;
                console.log(affichage);
                const paragraphe = document.createElement('p');
                paragraphe.textContent = affichage;
                resultat.append(paragraphe);
            })

        })
        
        .catch(function (error) {
            console.log("la requête n'a pas abouti");
        });
}


